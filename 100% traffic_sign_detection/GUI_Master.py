# -*- coding: utf-8 -*-


import tkinter as tk
from tkinter import ttk, LEFT, END
from PIL import Image, ImageTk
from tkinter.filedialog import askopenfilename
from tkinter import messagebox as ms
import cv2
import sqlite3
import os
import numpy as np
import time


global fn
fn = ""
##############################################+=============================================================
root = tk.Tk()
root.configure(background="brown")
# root.geometry("1300x700")


w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))
root.title("Traffic Sign Detection")

# 43

# ++++++++++++++++++++++++++++++++++++++++++++
#####For background Image
image2 = Image.open('s8.jpg')
image2 = image2.resize((1530, 900), Image.ANTIALIAS)

background_image = ImageTk.PhotoImage(image2)

background_label = tk.Label(root, image=background_image)

background_label.image = background_image

background_label.place(x=0, y=0)  # , relwidth=1, relheight=1)
#
label_l1 = tk.Label(root, text="Traffic Sign Detection System",font=("Times New Roman", 45, 'bold'),
                    background="black", fg="white", height=1)
label_l1.place(x=200, y=10)





def log():
    from subprocess import call
    call(["python","detect_sign.py"])
    
def window():
  root.destroy()


button1 = tk.Button(root, text="Detect Traffic Sign", command=log, width=14, height=1,font=('times', 20, ' bold '), bg="Yellow Green", fg="white")
button1.place(x=100, y=160)


button3 = tk.Button(root, text="Exit",command=window,width=14, height=1,font=('times', 20, ' bold '), bg="#FF0000", fg="white")
button3.place(x=100, y=240)

root.mainloop()